// ##### PAGE 1 - MAIN PAGE #####

$("#icon-down").on("click", function () {

    $('body, html').animate({
        scrollTop: $('#weather-page').offset().top -74
    }, 1500)
});




// ##### PAGE 2 - WEATHER #####

const APIkey = "c99bf0428316352dae9080618b42363e";
let myLatitude = 0;
let myLongitude = 0;

$('#enter-city__ok').click(function () {
    let city = $('#enter-city__input').val();
    getWeatherFromCityFound(city);
});

$('#icon-location').click(function () {

    let geolocation = navigator.geolocation;

    if (geolocation) {
        geolocation.getCurrentPosition(success, error);
    } else {
        alert("Unfortunately, Your browser does not support geolocation services.");
    }

    function success(position) {
        myLatitude = position.coords.latitude;
        myLongitude = position.coords.longitude;
        getWeatherFromGeolocation(myLatitude, myLongitude);
    }

    function error(error) {
        alert(`ERROR(${error.code}): ${error.message}. To use the application you must agree to access geolocation`);
    }
});

function getWeatherFromCityFound(city) {
    const weatherURL = `https://api.openweathermap.org/data/2.5/weather?q=${city}&lang=en&appid=${APIkey}`;
    getJSON(weatherURL);
}

function getWeatherFromGeolocation(myLat, myLongi) {
    const weatherURL = `https://api.openweathermap.org/data/2.5/weather?lat=${myLat}&lon=${myLongi}&lang=en&APPID=${APIkey}`;
    getJSON(weatherURL);
}


function getJSON(weatherURL) {

    document.getElementById('content-wrapper').className = "";

    $.getJSON(weatherURL).then(function (data) {

        let temperatureC = data.main.temp - 273.15;
        let temperatureCFloor = Math.floor(temperatureC);

        let timeSunriseUnix = data.sys.sunrise;
        let timeSunsetUnix = data.sys.sunset;

        let timeZone = data.timezone;
        let differencesTime = (timeZone - 3600) / 3600;
        let formattedTimeSunrise = convertUnixTime(timeSunriseUnix, differencesTime);
        let formattedTimeSunset = convertUnixTime(timeSunsetUnix, differencesTime);


        $('#city-not-found').css("display", "none");
        $('.container-fluid').css("padding-top", "14vh");
        $('.sun').css("height", "20px");
        $('#date').css("padding", "5px");
        $('#horizontal-line').addClass('horizontal-line-active')


        $("#name-city").html(data.name);
        $("#temperature").html(`${temperatureCFloor} *C`);
        $("#humidity").html(`Humidity: ${data.main.humidity}%`);
        $("#wind-speed").html(`Wind speed: ${data.wind.speed}m/s`);


        $(".icon-sun").css("display", "flex");
        $(".card-body").css("padding-top", "1.25rem");
        $("#time-sunrise").html(` : ${formattedTimeSunrise}`);
        $("#time-sunset").html(` : ${formattedTimeSunset}`);
        $("#sun").addClass('sun');

        switch (data.weather[0].icon.substr(0, 2)) {

            case "01":
                $('#content-wrapper').addClass("clear-sky");
                break;
            case "02":
                $('#content-wrapper').addClass("few-clouds");
                break;
            case "03":
                $('#content-wrapper').addClass("cloudy");
                break;
            case "04":
                $('#content-wrapper').addClass("cloudy");
                break;
            case "09":
                $('#content-wrapper').addClass("rain");
                break;
            case "10":
                $('#content-wrapper').addClass("rain");
                break;
            case "11":
                $('#content-wrapper').addClass("thunderstorm");
                break;
            case "13":
                $('#content-wrapper').addClass("snow");
                break;
            case "50":
                $('#content-wrapper').addClass("mist");
                break;
        }

    }).fail(function () {

        $('#city-not-found').css("display", "flex");

        setTimeout(function () {
            $('#city-not-found').css("display", "none");
        }, 3000);

    });
    document.getElementById('enter-city__input').value = '';

}

function convertUnixTime(timeUTC, differencesTime) {
    let date = new Date(timeUTC * 1000);
    let hours = date.getHours();
    let hoursPlusUTC = hours + differencesTime;

    if (hoursPlusUTC >= 24) {
        hoursPlusUTC = hoursPlusUTC - 24;
    }

    if (hoursPlusUTC < 0) {
        hoursPlusUTC = 24 + hoursPlusUTC;
    }

    let minutes = "0" + date.getMinutes();
    return `${hoursPlusUTC - 1}:${minutes.substr(-2)}`;
}









